const { ApolloServer, gql } = require("apollo-server");

let db = [
  {
    id: "123",
    text: "asdfafs",
    timestamp: () => new Date().toISOString()
  }
];

const typeDefs = gql`
  type Note {
    id: String!
    text: String!
    timestamp: String!
  }
  type Query {
    getNote(noteId: ID!): Note!
    getNotes: [Note!]!
  }
  type Mutation {
    createNote(text: String): Note
    deleteNote(noteId: String!): Note
  }
`;

const resolvers = {
  Query: {
    getNote: async (parent, args) => {
      try {
        const { noteId } = args;
        return db.find(item => item.id === noteId);
      } catch (error) {
        throw new Error(error);
      }
    },
    getNotes: async (parent, args) => {
      try {
        return db;
      } catch (error) {
        throw new Error(error);
      }
    },
  },

  Mutation: {
    createNote: async (parent, args) => {
      try {
        const { noteInput } = args;
        const now = new Date();
        db.push({
          id: `${Math.random()}`,
          text: noteInput,
          timestamp: now.toISOString()
        });
        return db;
      } catch (error) {
        throw new Error(error);
      }
    },
    deleteNote: async (parent, args) => {
      try {
        const { noteId } = args;
        return db = db.filter(item => item.id !== noteId);
      } catch (error) {
        throw new Error(error);
      }
    },
  },
};

const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});